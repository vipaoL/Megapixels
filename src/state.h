#pragma once
#include "dcp.h"
#include <libmegapixels.h>
#include <stdbool.h>

typedef struct cstate {
        uint32_t control;
        int32_t value;
        int32_t value_req;
        int32_t max;
        bool manual;
        bool manual_req;
        uint32_t auto_control;
} controlstate;

typedef struct state_main {
        libmegapixels_devconfig *configuration;
        libmegapixels_camera *camera;
        libmegapixels_aaa_stats stats;

        // Size of the preview widget
        int preview_width;
        int preview_height;

        // Size of the frame to draw in the preview widget
        int preview_buffer_width;
        int preview_buffer_height;

        int device_rotation;

        int burst_length;

        // Control state
        bool control_flash;
        bool flash_enabled;
        controlstate gain;
        controlstate exposure;
        controlstate focus;

        bool has_auto_focus_continuous;
        bool has_auto_focus_start;
} mp_state_main;

typedef struct state_io {
        libmegapixels_devconfig *configuration;
        libmegapixels_camera *camera;
        libmegapixels_mode *mode_capture;
        libmegapixels_mode *mode_preview;
        libmegapixels_aaa_stats stats;

        int burst_length;
        int captures_remaining;
        bool flush_pipeline;
        int blank_frame_count;

        // Control state
        controlstate gain;
        controlstate exposure;
        controlstate focus;
        controlstate red;
        controlstate blue;
        bool can_af_trigger;
        bool trigger_af;
        bool flash_enabled;

        // State passed through to the process pipeline
        int preview_width;
        int preview_height;
        int device_rotation;
} mp_state_io;

typedef enum aaa_mode {
        AAA_DISABLED,
        AAA_BY_SENSOR,
        AAA_BY_V4L2_CONTROLS,
        AAA_BY_POST,
} aaa_mode;

typedef struct state_proc {
        libmegapixels_devconfig *configuration;
        libmegapixels_camera *camera;
        libmegapixels_mode *mode;
        libmegapixels_aaa_stats stats;

        int burst_length;
        int captures_remaining;
        int counter;
        int preview_width;
        int preview_height;

        // Device orientation in degrees
        int device_rotation;
        // Camera orientation as number
        int camera_rotation;

        bool is_capturing;

        float balance[3];
        controlstate gain;
        controlstate exposure;
        controlstate focus;

        struct MPCameraCalibration calibration;

        float red;
        float blue;
        float blacklevel;

        int mode_balance;
        int mode_exposure;
        int mode_focus;
} mp_state_proc;