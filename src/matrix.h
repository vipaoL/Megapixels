#pragma once

static float IDENTITY[9] = {
        // clang-format off
        1, 0, 0,
        0, 1, 0,
        0, 0, 1,
        // clang-format on
};

static float XYZD50_to_D65[] = {
        // clang-format off
         0.9555766f,  -0.0230393f, 0.0631636f,
        -0.0282895f,   1.0099416f, 0.0210077f,
         0.0122982f,  -0.0204830f, 1.3299098f
        // clang-format on
};

static float XYZD65_to_sRGB[] = {
        // clang-format off
         3.2406f,  -1.5372f,   -0.4986f,
        -0.9689f,   1.8758f,    0.0415f,
         0.0557f,  -0.2040f,    1.0570f
        // clang-format on
};

static float YUV_to_RGB[] = {
        // clang-format off
         0.299f,  0.587f,   0.114f,
        -0.299f, -0.587f,   0.886f,
         0.701f, -0.597f,  -0.114f
        // clang-format on
};

void print_matrix(float m[9]);

void multiply_matrices(const float a[9], const float b[9], float out[9]);

void invert_matrix(const float in[9], float out[9]);

void transpose_matrix(const float in[9], float out[9]);